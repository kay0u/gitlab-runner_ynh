### To remove all runners from this project:

Create a file with:

```bash
# https://docs.gitlab.com/ee/api/runners.html
PRIVATE_TOKEN="XXXXXX" # https://gitlab.com/kay0u/gitlab-runner_ynh/-/settings/access_tokens
PROJECT_ID="17616325"

runners=$(curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/runners?type=project_type&per_page=1000" | jq '.[].id') #| xargs -I runner_id curl --request DELETE --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/runners/runner_id"

echo $runners

for r in $runners
do
    curl --request DELETE --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/runners/$r"&
done
```

Create an access token https://gitlab.com/kay0u/gitlab-runner_ynh/-/settings/access_tokens, and run it